#! /usr/bin/env python

import json
import os
import socket
import sys
from time import time as ts

import requests
from netaddr import AddrFormatError, IPAddress, IPNetwork


def info(msg):
    print(f"\033[32m[INFO]\033[0m    {msg}")


def warning(msg):
    print(f"\033[33m[WARNING]\033[0m {msg}")


def error(msg):
    print(f"\033[31m[ERROR]\033[0m   {msg}")


def fatal(msg):
    print(f"\033[35m[FATAL]\033[0m   {msg}")
    sys.exit(1)


def refresh_ip_ranges():
    r = requests.get("https://ip-ranges.amazonaws.com/ip-ranges.json")
    if not r.ok:
        raise Exception(f"Error {r.status_code}")

    with open("ip-ranges.json", "w") as f:
        f.write(r.text)


def get_ip_ranges():
    if not os.path.exists("ip-ranges.json"):
        info("ip-ranges.json not found. Downloading.")
        refresh_ip_ranges()
    elif ts() - os.path.getmtime("ip-ranges.json") > 86400:
        info("ip-ranges.json outdated. Refreshing.")
        refresh_ip_ranges()
    else:
        info("Using cached ip-ranges.json.")

    with open("ip-ranges.json") as f:
        ranges = json.load(f)

    return [IPNetwork(p["ip_prefix"]) for p in ranges["prefixes"]]


def get_requested_ips():
    if len(sys.argv) < 2:
        fatal(f"Usage: {sys.argv[0]} <IP|URL> ...")

    ips_to_check = []

    for arg in sys.argv[1:]:
        try:
            ip = IPAddress(arg)
        except AddrFormatError:
            pass
        else:
            ips_to_check.append(ip)
            continue

        try:
            ip = socket.gethostbyname(arg)
        except socket.error:
            error(f"Invalid hostname: {arg}")
        else:
            ips_to_check.append(IPAddress(ip))

    return ips_to_check


def get_network(ip, networks):
    for n in networks:
        if ip in n:
            return n
    else:
        error(f"IP {ip} not in any network.")


def main():
    ips_to_check = get_requested_ips()
    if not ips_to_check:
        fatal("No IPs specified")

    ranges = get_ip_ranges()

    networks = []

    for ip in ips_to_check:
        n = get_network(ip, ranges)
        if n:
            networks.append(n)

    if not networks:
        return

    info("Networks")
    for n in sorted(set(networks)):
        print(f"    {n}")


if __name__ == "__main__":
    main()
